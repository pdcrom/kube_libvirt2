#!/bin/sh

init_master () {

priv_ip=$(ip -f inet -o addr show eth0|cut -d\  -f 7 | cut -d/ -f 1 | head -n 1)
sed -i "s/advertiseAddress.*/advertiseAddress: \"$priv_ip\"/" ${REMOTE_INSTALL_DIR}/${KUBEADM_MASTER}
/opt/bin/kubeadm init --config ${REMOTE_INSTALL_DIR}/${KUBEADM_MASTER} --ignore-preflight-errors=FileExisting-conntrack

mkdir -p /home/core/.kube
mkdir -p /root/.kube
cp -f /etc/kubernetes/admin.conf /root/.kube/config
cp -f /etc/kubernetes/admin.conf /home/core/.kube/config
chown core:core /home/core/.kube/config

crictl config runtime-endpoint unix:///run/containerd/containerd.sock

export KUBECONFIG=/etc/kubernetes/admin.conf

kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
# kubectl patch -n kube-system daemonset weave-net --patch "$(< /tmp/install/weave-patch-log-level.yaml)"
}


init_worker () {
    echo 
}

## only run the installer on the firstboot
if [ -d "/opt/bin/cni" ]; then
  # Control will enter here if $DIRECTORY exists.
  echo "installer will only run at first boot"
  exit 0
fi

# https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/_print/
# https://suraj.io/post/2021/01/kubeadm-flatcar/

PATH=$PATH:/opt/bin

mkdir -p /opt/bin
mkdir -p /opt/cni/bin
#Install CNI plugins (required for most pod network):
curl -Ls "https://github.com/containernetworking/plugins/releases/download/${CNI_VERSION}/cni-plugins-linux-amd64-${CNI_VERSION}.tgz" | tar -C /opt/cni/bin -xz
#Install crictl (required for kubeadm / Kubelet Container Runtime Interface (CRI))
curl -Ls "https://github.com/kubernetes-sigs/cri-tools/releases/download/${CRICTL_VERSION}/crictl-${CRICTL_VERSION}-linux-amd64.tar.gz" | tar -C /opt/bin -xz
#Install kubeadm, kubelet, kubectl and add a kubelet systemd service:
#RELEASE="$(curl -sSL https://dl.k8s.io/release/stable.txt)"
cd /opt/bin
curl -Ls --remote-name-all https://storage.googleapis.com/kubernetes-release/release/${RELEASE}/bin/linux/amd64/{kubeadm,kubelet,kubectl}
chmod +x {kubeadm,kubelet,kubectl}

curl -sSL "https://raw.githubusercontent.com/kubernetes/release/${RELEASE_VERSION}/cmd/kubepkg/templates/latest/deb/kubelet/lib/systemd/system/kubelet.service" | sed "s:/usr/bin:/opt/bin:g" > /etc/systemd/system/kubelet.service
mkdir -p /etc/systemd/system/kubelet.service.d
curl -sSL "https://raw.githubusercontent.com/kubernetes/release/${RELEASE_VERSION}/cmd/kubepkg/templates/latest/deb/kubeadm/10-kubeadm.conf" | sed "s:/usr/bin:/opt/bin:g" > /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

systemctl enable --now kubelet

echo "Started kubelet"

#allow pull from private gitlab
mkdir -p /var/lib/kubelet
cp ${REMOTE_INSTALL_DIR}/docker_config.json /var/lib/kubelet/config.json

if [ $1 == 'master' ]
then
  init_master > /var/log/init_master.log
elif [ $1 == 'worker' ]
then
  init_worker > /var/log/init_worker.log
else
exit 1
fi
