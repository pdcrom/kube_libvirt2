# disable update engine
data "ignition_systemd_unit" "update-engine" {
  name = "update-engine.service"
  mask = true
}

# disable locksmith
data "ignition_systemd_unit" "locksmithd" {
  name = "locksmithd.service"
  mask = true
}

# add local docker registry
data "ignition_systemd_unit" "docker" {
  name    = "docker.service"
  enabled = true
  dropin {
    name    = "50-insecure-registry.conf"
    content = "[Service]\nEnvironment='DOCKER_OPTS=--insecure-registry=dockerrepo:5000'"
  }
}

# set hostname
data "ignition_file" "hostname_worker" {
  count      = var.node_count
  # filesystem = "root" # default `ROOT` filesystem
  path       = "/etc/hostname"
  mode       = 420 # octal 0644

  content {
    content = "${var.hostname_prefix}${var.node_type}${count.index}"
  }
}

# set ssh key
data "ignition_user" "core" {
  name = "core"

  ssh_authorized_keys = [
    file("${var.config.node_public_key_file}")
  ]
}

# data "ignition_networkd_unit" "network" {
#   count = var.node_count

#   name    = "00-eth0.network"
#   content = "[Match]\nName=eth0\n\n[Network]\nDNS=${var.config.network_dns}\nGateway=${var.config.network_gateway}\nAddress=${var.config.network_prefix}${var.network_ip_offset + count.index}/24"
# }

data "ignition_file" "network_worker" {
  count      = var.node_count
  path       = "/etc/systemd/network/00-eth0.network"
  mode       = 420 # decimal 0644

  content {
    content = <<-EOT
    [Match]
    Name=eth0

    [Network]
    LinkLocalAddressing=no
    IPv6AcceptRA=no
    DNS=${var.config.network_dns}
    Gateway=${var.config.network_gateway}
    Address=${var.config.network_prefix}${var.network_ip_offset + count.index}/24
    EOT
  }
}

data "ignition_directory" "install" {
  path   = var.config.remote_install_dir
}

data "ignition_file" "install_script" {
  path       = "${var.config.remote_install_dir}/${var.config.install_script}"
  mode       = 484 # octal=0744

  content {
    content = templatefile("${var.config.local_install_dir}/${var.config.install_script}", { REMOTE_INSTALL_DIR=var.config.remote_install_dir, RELEASE=var.config.k8s_version, RELEASE_VERSION=var.config.template_version,CNI_VERSION=var.config.cni_version, CRICTL_VERSION=var.config.crictl_version, KUBEADM_MASTER=var.config.kubeadm_master})
  }
}

data "ignition_file" "kubeadm_master" {
  path       = "${var.config.remote_install_dir}/${var.config.kubeadm_master}"
  mode    = 420 # octal=0644

  content {
      content  = file("${var.config.local_install_dir}/${var.config.kubeadm_master}")
  }

}

data "ignition_file" "docker_config" {
  path       = "${var.config.remote_install_dir}/${var.config.docker_config}"
  mode   = 420 # octal=0644

  content {
    content = file("${var.config.local_install_dir}/${var.config.docker_config}")
  }
}

# build the ignition config
data "ignition_config" "ignition_worker" {
  count = var.node_count

  users = [
    data.ignition_user.core.rendered,
  ]

  directories = [
      data.ignition_directory.install.rendered
  ]

  files = [
    "${element(data.ignition_file.hostname_worker.*.rendered, count.index)}",
    "${element(data.ignition_file.network_worker.*.rendered, count.index)}",
    data.ignition_file.install_script.rendered, 
    data.ignition_file.kubeadm_master.rendered, 
    data.ignition_file.docker_config.rendered
  ]

  systemd = [
    data.ignition_systemd_unit.update-engine.rendered,
    data.ignition_systemd_unit.locksmithd.rendered,
    data.ignition_systemd_unit.docker.rendered,
  ]
}

resource "libvirt_ignition" "ignition_worker" {
  count = var.node_count
  name  = "${var.hostname_prefix}${var.node_type}${count.index}.ign"
  pool  = var.config.volume_pool
  # content = data.ignition_config.ignition_worker.*.rendered[${count.index}]
  content = "${element(data.ignition_config.ignition_worker.*.rendered, count.index)}"
}
