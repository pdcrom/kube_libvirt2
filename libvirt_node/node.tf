resource "libvirt_volume" "node-disk" {
  count            = var.node_count
  name             = "${var.hostname_prefix}${var.node_type}${count.index}.qcow2"
  pool             = var.config.volume_pool
  base_volume_name = var.config.base_volume_name
  base_volume_pool = var.config.base_volume_pool
  size             = var.disk_size
}

resource "libvirt_domain" "node" {

  count  = var.node_count
  name   = "${var.hostname_prefix}${var.node_type}${count.index}"
  memory = var.memory
  vcpu   = var.vcpu

  coreos_ignition = libvirt_ignition.ignition_worker.*.id[count.index]
  fw_cfg_name     = "opt/org.flatcar-linux/config"
  network_interface {
    network_name   = var.config.network_name
    wait_for_lease = var.config.network_dhcp
  }

  disk {
    volume_id = libvirt_volume.node-disk.*.id[count.index]
  }

  graphics {
    type           = "vnc"
    listen_type    = "address"
    autoport       = true
    listen_address = "0.0.0.0"
  }

  connection {
    type        = "ssh"
    user        = "core"
    host        = var.config.network_dhcp ? "${self.network_interface.0.addresses.0}" : "${var.config.network_prefix}${var.network_ip_offset + count.index}"
    private_key = "${file(var.config.node_private_key_file)}"

    bastion_host        = var.config.bastion_host
    bastion_user        = var.config.bastion_user
    bastion_private_key = var.config.bastion_private_key_file != "" ? "${file(var.config.bastion_private_key_file)}" : ""
  }

  # provisioner "file" {
  #   content      = templatefile("${var.config.install_folder}/install.sh", { RELEASE=var.config.k8s_version, RELEASE_VERSION=var.config.template_version,CNI_VERSION=var.config.cni_version, CRICTL_VERSION=var.config.crictl_version})
  #   destination = "/tmp/install/install.sh"
  # }

  # provisioner "file" {
  #   source      = "${var.config.install_folder}/docker_config.json"
  #   destination = "/tmp/install/docker_config.json"
  # }
  # provisioner "file" {
  #   source      = "${var.config.install_folder}/kubeadm_master.yaml"
  #   destination = "/tmp/install/kubeadm_master.yaml"
  # }

  provisioner "remote-exec" {
    inline = [
      # "sudo chmod +x /opt/install/${var.config.install_script}",
      "sudo ${var.config.remote_install_dir}/${var.config.install_script} ${var.node_type}"
    ]
  }
}

output "ips" {

  #return the ip addresses of all nodes provisioned
  value = var.config.network_dhcp ? [for x in libvirt_domain.node : x.network_interface[0].addresses[0]] : [for x in range(var.node_count) : "${var.config.network_prefix}${var.network_ip_offset + x}"]
}

