terraform {
  required_providers {
    ignition = {
      source = "community-terraform-providers/ignition"
      # version = "3.0.0"
    }
    libvirt = {
      source = "dmacvicar/libvirt"
      version = "0.7.0"
    } 
  }
}
