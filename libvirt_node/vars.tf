variable "node_count" { default = 1 }

variable "node_type" { default = "master" }

variable "memory" { default = 1024 }

variable "vcpu" { default = 2 }

variable "disk_size" { default = 10737418240 } # 10 GB

variable "hostname_prefix" { default = "" }

variable "network_ip_offset" { default = "" } #used to prefix the nodes 4th digit in the IP address

variable "config" {
  default = {}
}
