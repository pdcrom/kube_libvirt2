provider "libvirt" {
   uri = local.config.libvirt_uri
}

resource "libvirt_network" "kube_network" {
  count = local.config.network_create ? 1 : 0

  name      = local.config.network_name
  mode      = local.config.network_mode
  domain    = local.config.network_domain
  addresses = local.config.network_addresses
  dns {
    enabled = true
  }
}

module "master" {
  source            = "./libvirt_node"
  node_type         = "master"
  config            = local.config
  node_count        = local.config.master_count
  hostname_prefix   = local.config.hostname_prefix
  vcpu              = local.config.master_vcpu
  memory            = local.config.master_memory
  network_ip_offset = local.config.master_ip_offset
  disk_size         = local.config.master_disk_size
}

module "worker" {
  source            = "./libvirt_node"
  node_type         = "worker"
  config            = local.config
  node_count        = local.config.worker_count
  hostname_prefix   = local.config.hostname_prefix
  vcpu              = local.config.worker_vcpu
  memory            = local.config.worker_memory
  network_ip_offset = local.config.worker_ip_offset
  disk_size         = local.config.worker_disk_size
}

output "master_ips" {
  value = module.master.ips
}

output "worker_ips" {
  value = module.worker.ips
}

resource "null_resource" "download-config" {

  triggers = {
    always_run = "${timestamp()}"
  }

  depends_on = [module.master]

  provisioner "local-exec" {
    command = <<EOT
      eval ${local.set_ssh_flags}
      mkdir tmp
      eval "scp $ssh_flags $ProxyCommand core@${module.master.ips[0]}:~/.kube/config tmp/admin.config"
      cmd="'/opt/bin/kubeadm token create --print-join-command' > tmp/join-worker.sh"
      eval "ssh $ssh_flags $ProxyCommand core@${module.master.ips[0]} $cmd"

      #check for OSX
      uname -a | grep Darwin
      if [ $? -eq 0 ]
        then
          #use OSX sed syntax
          sed -i '' 's/$/ --ignore-preflight-errors=FileExisting-conntrack/' tmp/join-worker.sh
        else
          #use GNU syntax  
          sed -i 's/$/ --ignore-preflight-errors=FileExisting-conntrack/' tmp/join-worker.sh
        fi 
  EOT    
  }
}

resource "null_resource" "join-workers" {

  count      = local.config.worker_count
  depends_on = [null_resource.download-config, module.worker]

  provisioner "local-exec" {
    command = <<EOT
      eval ${local.set_ssh_flags}
      eval "scp $ssh_flags $ProxyCommand tmp/join-worker.sh core@${module.worker.ips[count.index]}:/tmp"
      cmd='export PATH=$PATH:/opt/bin && chmod a+x /tmp/join-worker.sh && sudo /tmp/join-worker.sh'
      eval "ssh $ssh_flags $ProxyCommand core@${module.worker.ips[count.index]} '$cmd'"
  EOT    
  }
}

resource "null_resource" "wait-for-nodes-ready" {

  triggers = {
    always_run = "${timestamp()}"
  }
  
  depends_on = [module.master]

  provisioner "local-exec" {
    command = <<EOT
      eval ${local.set_ssh_flags}
      cmd='${templatefile("./scripts/wait_nodes_ready.sh", { num_nodes = local.config.master_count + local.config.worker_count })}'
      eval "ssh $ssh_flags $ProxyCommand core@${module.master.ips[0]} '$cmd'"
    EOT    
  }
}

# resource "null_resource" "install-flux" {

#   count      = local.config.install_flux ? 1 : 0
#   depends_on = [null_resource.wait-for-nodes-ready]

#   provisioner "local-exec" {
#     command = <<EOT
#       eval ${local.set_ssh_flags}
#       cmd='${templatefile("./scripts/install_flux.sh", { PRIVATEKEY = local.config.flux_private_key })}'
#       eval "ssh $ssh_flags $ProxyCommand core@${module.master.ips[0]} '$cmd'"
#     EOT    
#   }
# }

