export PATH=$PATH:/opt/bin

# download fluxctl
curl -Ls --remote-name-all https://github.com/fluxcd/flux/releases/download/1.19.0/fluxctl_linux_amd64
mv fluxctl_linux_amd64 fluxctl
chmod a+x fluxctl

# download helm and add fluxcd repo
curl -Ls https://get.helm.sh/helm-v3.2.1-linux-amd64.tar.gz | tar -xz --strip-components=1 linux-amd64/helm
./helm repo add fluxcd https://charts.fluxcd.io

# create namespace
kubectl create ns flux

# create secret from docker-config
kubectl create secret generic flux-docker-config -n flux --from-file=config.json=/tmp/install/docker_config.json

# create secret from flux private key
kubectl create secret generic flux-git-deploy -n flux --from-file=identity=/tmp/install/${PRIVATEKEY}

# install flux
./helm upgrade -i --wait flux fluxcd/flux -n flux --values /tmp/install/helm-flux-values.yaml

# install Helm operator
./helm upgrade -i --wait helm-operator fluxcd/helm-operator -n flux --values /tmp/install/helm-operator-values.yaml

#install sealed secrets in kube-system
kubectl apply -f https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.12.3/controller.yaml      