n_nodes=${num_nodes}
n=0
while [ $n -lt $n_nodes ]
do
  echo "Wait for all "$n_nodes" kubernetes nodes to be Ready (sleep 20 sec)"
  /opt/bin/kubectl get nodes
  n=$(/opt/bin/kubectl get nodes -o "jsonpath={range .items[*]}{@.metadata.name}:{range @.status.conditions[?(@.type==\"Ready\")]}{@.type}={@.status}{\"\n\"}{end}{end}" | grep True | wc -l)
  echo  $n - $n_nodes
  sleep 10
done